package chess;

public class Board {
    Piece[][] _positions;

    Board()
    {
        _positions = new Piece[8][8];
        intialize();

    }

    /***
     * initializes the chess board
     */
    private void intialize()
    {
        //blue team
        _positions[0][0] = new Rook((byte)0, (byte)0, false);
        _positions[1][0] = new Knight((byte)1, (byte)0, false);
        _positions[2][0] = new Bishop((byte)2, (byte)0, false);
        _positions[3][0] = new King((byte)3, (byte)0, false);
        _positions[4][0] = new Queen((byte)4, (byte)0, false);
        _positions[5][0] = new Bishop((byte)5, (byte)0, false);
        _positions[6][0] = new Knight((byte)6, (byte)0, false);
        _positions[7][0] = new Rook((byte)7, (byte)0, false);

        for(byte i=0; i<8; i++)
        {
            _positions[i][1] = new Pawn(i, (byte)1, false, this);
        }

        //red team
        _positions[0][7] = new Rook((byte)0, (byte)0, true);
        _positions[1][7] = new Knight((byte)1, (byte)0, true);
        _positions[2][7] = new Bishop((byte)2, (byte)0, true);
        _positions[3][7] = new King((byte)3, (byte)0, true);
        _positions[4][7] = new Queen((byte)4, (byte)0, true);
        _positions[5][7] = new Bishop((byte)5, (byte)0, true);
        _positions[6][7] = new Knight((byte)6, (byte)0, true);
        _positions[7][7] = new Rook((byte)7, (byte)0, true);

        for(byte i=0; i<8; i++)
        {
            _positions[i][6] = new Pawn(i, (byte)7, true, this);
        }
    }

    
    public void printBoard()
    {
        for(byte i=0; i<8; i++)
        {
            System.out.print(8-i+"    ");

            for(byte j=0; j<8; j++)
            {
                if(_positions[j][i]==null)
                {
                    System.out.print(" ");
                }
                else
                {
                    System.out.print(_positions[j][i].getVisual());
                }
                System.out.print(" ");
            }
            System.out.print("\n");
        }
        System.out.println("\n     A B C D E F G H");
    }

    
    public Piece getPiece(byte[] coordinate)
    {
        return _positions[coordinate[0]][coordinate[1]];
    }

	public void setPiece(byte[] coordinate, Piece piece)
	{
		_positions[coordinate[0]][coordinate[1]] = piece;
	}

	
	/**
	 * checks if a piece is in the way of another piece when moving
	 * @param piece
	 * @param destination
	 * @return 
	 */
	public boolean getIsPathOpen(byte[] piece, byte[] destination) //TODO not working correctly
	{
		byte[] parameter = new byte[2];
		parameter[0] = (byte)(destination[0] - piece[0]);
		parameter[1] = (byte)-(destination[1] - piece[1]);
		
		do
		{
			if(parameter[0] != 0)
			{
				if(parameter[0] > 0)
				{
					parameter[0]--;
				}
				else
				{
					parameter[0]++;
				}
			}
			
			if(parameter[1] != 0)
			{
				if(parameter[1] > 0)
				{
					parameter[1]--;
				}
				else
				{
					parameter[1]++;
				}
			}
			
			if(getPiece(parameter)!=null)
			{
				return false;
			}
		}
		while(parameter[0]!=0 && parameter[1]!=0);
		
		return true;
	}
}
