package chess;

public class Rook extends Piece
{
    Rook(byte x, byte y, boolean whiteTeam)
    {
        super(x, y, whiteTeam);
        _visual = '♜';
    }

    /***
     * moves the rook to a new position. If he is able to reach the new positon true is returned otherwise false is returned.
     * @param destination[0]
     * @param destination[1]
     * @return
     */
    @Override
    public boolean moveTo(byte[] destination) //TODO stop walking over pieces
    {
    	if(destination[0]<0 || destination[0]>7 || destination[1]<0 || destination[1]>7)
    	{
    		return false;
    	}
        
        if(_coordiate[0] == destination[0] ^ _coordiate[1] == destination[1])
        {
            return true;
        }

        
        else
        {
            return false;
        }
    }
}
