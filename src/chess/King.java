package chess;

public class King extends Piece
{
    King(byte x, byte y, boolean whiteTeam)
    {
        super(x, y, whiteTeam);
        _visual = '♚';
    }

    /***
     * moves the queen to a new position. If he is able to reach the new positon true is returned otherwise false is returned.
     * @param destination[0]
     * @param destination[1]
     * @return
     */
    @Override
    public boolean moveTo(byte[] destination)
    {
    	if(destination[0]<0 || destination[0]>7 || destination[1]<0 || destination[1]>7)
    	{
    		return false;
    	}

        if(destination[0] == _coordiate[0]+1 || destination[0] == _coordiate[0]-1 || destination[1] == _coordiate[1]+1 || destination[1] == _coordiate[1]-1)
        {
            return true;
        }

        else
        {
            return false;
        }
    }
}
