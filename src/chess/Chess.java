package chess;

import java.util.Scanner;

public class Chess {
	private static Board _board = new Board();
    private static Scanner sc = new Scanner(System.in);
    private static boolean _gameover = false;


	public static void main(String[] args)
	{
		startScreen();
		//test();
	}
	
	
	
	
	private static void test() {
		makePlayerTurn(true);
		
	}




	private static void startScreen()
	{
		boolean accepted = false;
		
		do
		{
			System.out.println("Start game? y/N");
			char input = getInputChar();
			if(input == 'y')
			{
				accepted = true;
				menu();
			}
			else if(input == 'n')
			{
				accepted = true;
				exit((byte)0);
			}
		}
		while(!accepted);
	}
	
	private static void menu()
	{
		boolean accepted = false;
		
		do
		{
			System.out.println("MENU\n\n1.\tsingle player\n2.\tmulti player\n3.\tquit\n\nSelect 1/2/3");
			switch(sc.nextByte())
			{
			case 1:
				accepted=true;
				singeplayer();
				break;
				
			case 2:
				accepted=true;
				multiplayer();
				break;
				
			case 3:
				accepted=true;
				exit((byte)0);
				break;
			}
		}
		while(!accepted);
	}
	
	
	private static void multiplayer()
	{
		_gameover = false;
		int gameStatus = 0; //0=red turn 1=blue turn 2=red winner 3=blue winner 4=draw
		System.out.println("MULTIPLAYER\n\n");
		_board.printBoard();
		do
		{
			if(gameStatus==0)
			{
				makePlayerTurn(true);
			}
			
			else if(gameStatus==1)
			{
				makePlayerTurn(false);
			}
			
			_board.printBoard();
			
			if(_gameover)
			{
				gameStatus += 2;
			}
			else
			{
				gameStatus = (gameStatus+1)%2;
			}
		}
		while (gameStatus <2);
		
		
		System.out.println();
		if(gameStatus==2)
		{
			System.out.println("red team wins");
		}
		else if(gameStatus==3)
		{
			System.out.println("blue team wins");
		}
		else
		{
			System.out.println("draw");
		}
		
		System.out.println("\ndo you want to:\n1.\tstart new game\n2.\treturn to menu");
		boolean accept = false;
		do
		{
			switch(sc.nextByte())
			{
			case 1:
				accept = true;
				multiplayer();
				break;
			case 2:
				accept = true;
				menu();
				break;
			}
		}
		while(!accept);
	}


	private static void singeplayer() {
		// TODO Auto-generated method stub
		
	}
	
	private static void makePlayerTurn(boolean redTurn) {
		String player;
		if(redTurn)
		{
			player="Red";
		}
		else
		{
			player="Blue";
		}
		
		Piece figure;
		byte[] destination;
		byte[] piece;
		
		boolean valideMove;
		do
		{
			valideMove = false;
			
			System.out.println(player+" player choose a piece to move");
			piece = getCoordinate();
			figure = _board.getPiece(piece);
			
			if(figure!=null && figure.isRedTeam() == redTurn)
			{
				valideMove = true;
			}			
			
			System.out.println(player+" player choose where to move the piece");
			destination = getCoordinate();
			Piece target = _board.getPiece(destination);
			if(target != null && target.isRedTeam() == redTurn)
			{
				valideMove = false;
			}
			
			if(!(_board.getPiece(piece) instanceof Knight) && !_board.getIsPathOpen(piece, destination))
			{
				valideMove = false;
			}
			
			if(!valideMove)
			{
				System.out.println("illegal move. Try aggain\n");
			}
		}
		while(!valideMove);
		if(_board.getPiece(destination) instanceof King)
		{
			_gameover=true;
		}
		
		_board.setPiece(destination, figure);
		_board.setPiece(piece, null);
		
	}
	
	public static byte[] getCoordinate() //TODO check for wrong input Bsp. A
	{
		String input;
		do
		{
			input = sc.nextLine();
		}
		while (input=="");
		
		byte[] move = new byte[2];
		int number = (byte)7;
        int letter = (byte) input.toLowerCase().charAt(0) - 97;
        number -= (byte)input.charAt(1) - 49;
        move[0] = (byte)letter;
        move[1] = (byte)number;
        
        return move;
	}


	private static char getInputChar()
	{
		String input = sc.nextLine();
		input.toLowerCase();
		return input.charAt(0);
	}
	
	
	private static void exit(byte n)
	{
		System.exit(n);
	}
}
