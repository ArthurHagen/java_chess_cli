package chess;

public abstract class Piece {
    protected byte[] _coordiate = new byte[2];
    protected boolean _redTeam;
    protected char _visual;

    Piece(byte x, byte y, boolean redTeam)
    {
    	_coordiate[0] = x;
    	_coordiate[1] = y;
        _redTeam = redTeam;
    }


    /***
     * set's _visual
     * @param visual
     */
    public void setVisual(char visual)
    {
        _visual = visual;
    }

    
    /***
     * returns _visual
     * @return
     */
    public String getVisual()
    {
        String output="";

        if(_redTeam)
        {
            output = "\033[31m" + _visual + "\033[0m";  //red
        }
        else
        {
            output = "\033[34m" + _visual + "\033[0m";  //blue
        }
        return output;
    }
    
    
    /***
     * sets the position of the piece
     * @param coordinate
     */
    public void setPosition(byte[] coordinate)
    {
    	_coordiate = coordinate;
    }
    
    
    /***
     * returns weather the piece is part of red team
     * @return
     */
    public boolean isRedTeam()
    {
    	return _redTeam;
    }


    /***
     * moves the piece to a position
     * @param destination
     * @return
     */
    public abstract boolean moveTo(byte[] destination);
}
