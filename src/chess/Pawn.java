package chess;

public class Pawn extends Piece {
	Board _board;
	
    Pawn(byte x, byte y, boolean redTeam, Board board)
    {
        super(x, y, redTeam);
        _board = board;
        _visual = '♟';
    }

    /***
     * moves the pawn to a new position. If he is able to reach the new position true is returned otherwise false is returned.
     * @param destination
     * @return
     */
    @Override
    public boolean moveTo(byte[] destination) //TODO allow to move 2 on first move (check for row and team)
    {
    	boolean attacking=false;
    	if(_board.getPiece(destination) != null)
    	{
    		attacking=true;
    	}
    	
    	if(destination[0]<0 || destination[0]>7 || destination[1]<0 || destination[1]>7)
    	{
    		return false;
    	}

    	
        byte direction;

        if(_redTeam)
        {
            direction = -1;
        }

        else
        {
            direction=1;
        }
        if(attacking)
        {
        	//when attacking
            if((destination[0] == _coordiate[0]+1 || destination[0] == _coordiate[0]-1) && destination[1] == _coordiate[1]+direction)
            {
                return true;
            }
        }
        else
        {
        	if((destination[0] == _coordiate[0] && destination[1] == _coordiate[1]+direction))
            {
                return true;
            }
        }
        
        return false;
    }
    
}
