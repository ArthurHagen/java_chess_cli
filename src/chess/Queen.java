package chess;

public class Queen extends Piece
{
    Queen(byte x, byte y, boolean whiteTeam)
    {
        super(x, y, whiteTeam);
        _visual = '♛';
    }

    /***
     * moves the queen to a new position. If he is able to reach the new positon true is returned otherwise false is returned.
     * @param destination[0]
     * @param destination[1]
     * @return
     */
    @Override
    public boolean moveTo(byte[] destination) //TODO take more efficient code from Bishop + stop walking over pieces
    {
    	if(destination[0]<0 || destination[0]>7 || destination[1]<0 || destination[1]>7)
    	{
    		return false;
    	}
    	if(_coordiate[0] == destination[0] ^ _coordiate[1] == destination[1])
        {
            return true;
        }
    	
    	else if(destination[0] < _coordiate[0] && destination[1] < _coordiate[1])
        {
        	while(destination[0] != _coordiate[0])
        	{
        		destination[0]++;
        		destination[1]++;
        	}	
        }
        else if(destination[0] < _coordiate[0] && destination[1] > _coordiate[1])
        {
        	while(destination[0] != _coordiate[0])
        	{
        		destination[0]++;
        		destination[1]--;
        	}	
        }
        
        else if(destination[0] > _coordiate[0] && destination[1] < _coordiate[1])
        {
        	while(destination[0] != _coordiate[0])
        	{
        		destination[0]--;
        		destination[1]++;
        	}	
        }
        
        else if(destination[0] > _coordiate[0] && destination[1] > _coordiate[1])
        {
        	while(destination[0] != _coordiate[0])
        	{
        		destination[0]--;
        		destination[1]--;
        	}	
        }
        
        if(destination[0] == _coordiate[0] && destination[1] == _coordiate[1])
        {
        	return true;
        }

        else
        {
            return false;
        }
    }
}
